// =====================Task_1 Change Background project======================

const changeBackgroundProject = () =>{
 const colors = ['yellow', 'red', 'green', 'blue', 'black', 'orange', 'pink'];
 let randColor = '';
 const buttonChangeBackground = document.querySelector(".btn_task_1");
 const backgroundBlock = document.querySelector(".tasc_1");

    buttonChangeBackground.addEventListener("click", () => {
        randColor = colors[Math.floor(Math.random()*colors.length)];
        backgroundBlock.style.backgroundColor = randColor;
    })
};

changeBackgroundProject();

// ==================Task_2 Displaying quotes of famous people======================

const quotesFamousPeople = () => {
    const phrase = [
        {
            'author' : 'Albert Einstein' ,
            'expression': 'I have no special talent. I am only passionately curious.'
        },
        {
            'author' : 'William Shakespeare' ,
            'expression': 'Wisely, and slow. They stumble that run fast.'
        },
        {
            'author' : 'Mother Teresa' ,
            'expression': 'If you judge people, you have no time to love them.'
        },
        {
            'author' : 'Buddha' ,
            'expression': 'All that we are is the result of what we have thought.'
        }

    ];

    let randPhrase =  [];
    const buttonPhrase = document.querySelector(".btn_task_2");
    const phrasedBlock = document.querySelector(".task_2_expressions");

    buttonPhrase.addEventListener("click", () => {
        randPhrase = phrase[Math.floor(Math.random()*phrase.length)];
        phrasedBlock.innerHTML  = randPhrase.author + ': ' +  randPhrase.expression;
    })
};

quotesFamousPeople();

//=========Task 3 Counter project===================================================//

const counterProject = () =>{
    const buttonPlus = document.querySelector(".counter_btn_plus");
    const buttonMinus = document.querySelector(".counter_btn_minus");
    const counterValue = document.querySelector(".counter_value");

    buttonPlus.addEventListener("click", () => {
        counterValue.innerHTML = parseInt(counterValue.innerHTML) + 1;
    });

    buttonMinus.addEventListener("click", () => {
       if(parseInt(counterValue.innerHTML) > 1){
           counterValue.innerHTML = parseInt(counterValue.innerHTML) - 1;
       }
    });

};

counterProject();
